class Constants{
  /*
  *Text for NoInternet BottomSheet
  * */
  static const String TEXT_NEED_INTERNET = 'Need Internet Access';
  static const String TEXT_REQUEST_INTERNET = 'Please Turn On your Internet';

  /*
  * Login Screen Static text
  * */
  static const String TEXT_LOGIN = 'Login';
  static const String TEXT_USER_NAME = 'Enter Email';
  static const String TEXT_PASSWORD = 'Enter Password';
  static const String TEXT_COUPON = 'Enter Coupon code';

  /*
  * Logout Dailog Static text
  * */
  static const String TEXT_YES = 'Yes';
  static const String TEXT_NO = 'No';
  static const String TEXT_OK = 'OK';
  static const String TEXT_YOU_WANT_TO_LOGOUT = 'Are you sure you want to logout';
  static const String TEXT_LOGOUT = 'Logout';

}