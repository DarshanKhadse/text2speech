List stories = [
  {
    "name": "First Stories",
    "story": "I hope not. \n You might pull a muscle. \n You need to start small in order to achieve something big like that.When it comes to learning English, what if I told you that you can understand big ideas with just a little bit of text? You do not need to wait several years to deal with complex concepts.Just because you are learning a language does not mean you need to limit your thinking.Stories are all about going beyond reality. It is no wonder that they let you understand big concepts with only a little bit of reading practice.But this works better when you’re reading better stories.I am talking about award-winning short stories, told using language easily understood by beginners. These will not only improve your English reading comprehension but also open your mind to different worlds."
        "\n \n If the non-flexible contents of the row (those that are not wrapped in Expanded or Flexible widgets) are together wider than the row itself, then the row is said to have overflowed. When a row overflows, the row does not have any remaining space to share between its Expanded and Flexible children. The row reports this by drawing a yellow and black striped warning box on the edge that is overflowing. If there is room on the outside of the row, the amount of overflow is printed in red lettering.",
  },

];

