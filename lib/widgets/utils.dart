import 'dart:io';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils
{
/*
  * Hide soft keyboard
  * */
  static void hideKeyBoard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  /*
  * Shared Preferences Method
  * */
  static Future<SharedPreferences> initSharedPreferences() async
  {
    return await SharedPreferences.getInstance();
  }

  static Future<bool> internetConnection() async {
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }

}