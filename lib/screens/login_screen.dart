import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:text2speech/screens/forget_password.dart';
import 'package:text2speech/screens/homepage_screen.dart';
import 'package:text2speech/screens/signup_screen.dart';
import 'package:text2speech/widgets/constants.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  /*
  * Global Instance of GlobalKey(FormState)
  * */
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  /*
  * Global Instance of GlobalKey(ScaffoldState)
  * */
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController loginController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController couponController = TextEditingController();

  /*
  * Global Instance of FocusNode
  * */
  FocusNode loginFocus = new FocusNode();
  FocusNode passwordFocus = new FocusNode();
  FocusNode couponFocus = new FocusNode();
  String email;
  String pwd;
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              color: Color(0xFF121212),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Container(
                color: Color(0xFFb2ebf2),
                height: 500.0,
                width: 500.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Container(
                        color: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              /*
                                      * Login text
                                      * */
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      Container(
                                        child: Text(
                                          "Login",
                                          style: TextStyle(
                                              fontSize: 42.0,
                                              fontFamily: 'Productsans',
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                      Text(
                                        'Text to speech',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: 'Productsans',
                                          color: Colors.blue,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4.0,
                              ),
                              const SizedBox(height: 50.0),

                              /*
                                      * Enter User Name
                                      * */
                              TextFormField(
                                focusNode: loginFocus,
                                controller: loginController,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: Constants.TEXT_USER_NAME,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                },
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                validator: (value) {
                                  Pattern pattern =
                                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regexp = RegExp(pattern);
                                  if (value.isEmpty) {
                                    return 'Please Enter Email';
                                  } else if (!regexp.hasMatch(value)) {
                                    return 'Please enter valid Email';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  email = value;
                                },
                              ),
                              SizedBox(
                                height: 18.0,
                              ),
                              /*
                                       * Enter Password
                                       * */
                              TextFormField(
                                focusNode: passwordFocus,
                                controller: passwordController,
                                obscureText: _obscureText,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: Constants.TEXT_PASSWORD,
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    child: new Icon(
                                      _obscureText
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: Colors.black54,
                                      size: 22.0,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(couponFocus);
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please Enter Password';
                                  } else if (value.length <= 7) {
                                    return 'Password should be greater than 8';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  pwd = value;
                                },
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  FlatButton(
                                    child: Text('Forget Password?'),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ForgetPassword()));
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Center(
                                child: RaisedButton(
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      print('Success');
                                      logIn(email, pwd);
                                    }
                                    //loggingDone();
//                                    Navigator.pushReplacement(
//                                        context,
//                                        MaterialPageRoute(
//                                            builder: (context) =>
//                                                HomePageScreen()));
                                  },
                                  elevation: 5,
                                  color: Colors.blue,
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0)),
                                  child: Container(
                                    width: 220.0,
                                    height: 46.0,
                                    padding: const EdgeInsets.fromLTRB(
                                        40, 10, 40, 10),
                                    child: Center(
                                      child: Text('LOGIN',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontFamily: 'Productsans',
                                              color: Colors.white)),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 18,
                              ),
                              Center(
                                child: RichText(
                                    text: TextSpan(
                                        text: 'Not Have an Account? ',
                                        style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.black,
                                        ),
                                        children: [
                                      TextSpan(
                                          text: 'SignUp Here!',
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blue),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SignUp()));
                                            }),
                                    ])),
                              ),
                              SizedBox(
                                height: 18,
                              ),
                              Center(
                                  child: Text(
                                'OR',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              )),
                              SizedBox(
                                height: 14,
                              ),
                              Center(
                                child: RaisedButton(
                                    child: Container(
                                      width: 190.0,
                                      height: 46.0,
                                      padding: const EdgeInsets.fromLTRB(
                                          40, 10, 40, 10),
                                      child: Center(
                                        child: Text('Guest Login',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontFamily: 'Productsans',
                                                color: Colors.white)),
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0)),
                                    color: Colors.blue,
                                    textColor: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  HomePageScreen()));
                                    }),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void loggingDone() async {
    final pref = await SharedPreferences.getInstance();
    pref.setBool("KEY_IS_LOGGING", true);
  }

  logIn(String mail, String pwd) async {
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: mail, password: pwd)
        .then((FirebaseUser) {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePageScreen()));
    }).catchError((e) {
      print(e);
    });
  }
}
