import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:text2speech/screens/login_screen.dart';
import 'package:text2speech/screens/profile_screen.dart';
import 'package:text2speech/widgets/data.dart';

class HomePageScreen extends StatefulWidget {
  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {

  ScrollController scrollController;
  DateTime currentDate = DateTime.now().add(Duration(days: 30));
  final FlutterTts flutterTts = FlutterTts();
  String current;
  String speak;
  String username;
  getProfile()async{
    FirebaseAuth auth = FirebaseAuth.instance;
    User user = auth.currentUser;
    DocumentReference doc = FirebaseFirestore.instance.collection('users').doc(user.uid);
    doc.get().then((value) {
      Map<String, dynamic> userData = {
        'full_name': value.data()['full_name'],
        'email': value.data()['email'],
        'contact': value.data()['contact'],
      };
      print(userData['full_name']);
      setState(() {
        username = userData['full_name'];
        print(username);
      });
    });

  }



  @override
  void initState() {

    super.initState();
    getProfile();
    scrollController = ScrollController();

  }


  @override
  Widget build(BuildContext context) {

    Future speakText() async{
      print(await flutterTts.getLanguages);
      await flutterTts.setLanguage("en-US");
      await flutterTts.setPitch(0);
      await flutterTts.setSpeechRate(1.0);
      await flutterTts.speak(speak);
      flutterTts.setProgressHandler((text, start, end, word) {
        print(word);
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Text To Speech",
          style: TextStyle(
            fontSize: 16.0
          ),
        ),
      ),
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: MediaQuery.of(context).size.height/1.5,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  reverse: true,
                  controller: scrollController,
                  child: Column(
                    children: [
                      ListView.builder(
                        reverse: true,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: stories.length,
                        itemBuilder: (context, index) {
                          speak = stories[index]['story'].toString();
                          return Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Text(stories[index]['story'].toString(),
                                  style: TextStyle(
                                      fontSize: 20.0
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height:80.0)
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top:68.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.values[0],
              children: [
                Container(height:20.0,color: Color.fromRGBO(255, 255, 0, 0.3)),
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Tooltip(
                    decoration: BoxDecoration(color: Colors.grey,borderRadius: BorderRadius.circular(6)),
                    waitDuration: Duration(seconds: 0),
                    message: 'Pause is not currently supported for Android.',
                    child: RaisedButton(
                      child: Text(
                        'Stop',
                      ),
                      onPressed: (){
                        setState(() {
                          flutterTts.pause();
                          flutterTts.stop();
                          scrollController.animateTo(0.0,
                              duration: Duration(milliseconds: 1000000),
                              curve: Curves.linear);
                        });

                      },
                    ),
                  ),
                  RaisedButton(
                      child: Text(
                        'start',
                      ),
                      onPressed: (){
                        setState(() {
                          speakText();
                          scrollController.animateTo(0.0,
                              duration: Duration(milliseconds: 16000),
                              curve: Curves.linear);
                        });
                      }
                  )
                ],
              ),
            ],
          )
        ],
      ),
      /*
        * Navigation drawer
        * */
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(color: Colors.cyan),
              child:  Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [
                      GestureDetector(
                        onTap:(){
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => ProfileScreen()));
                        },
                        child: CircleAvatar(
                          radius: 30.0,
                          backgroundColor: Colors.white,
                          child: Text(
                            username == null ? '!' : username.substring(0,1),
                            style: TextStyle(fontSize: 22.0, color: Colors.cyan),
                          ),
                        ),
                      ),
                      SizedBox(width: 10.0,),
                      Text(
                        username ?? 'Guest User',
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 24.0, color: Colors.white, fontFamily: 'ProductSans'),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            ListTile(
              title: Text('Basic',
                  style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'ProductSans',
                      fontWeight: FontWeight.normal,
                      color: Colors.black)),
            ),

            ListTile(
              leading: Icon(
                Icons.home,
              ),
              title: Text('Home', style: TextStyle(fontSize: 16.0, fontFamily: 'ProductSans')),
              onTap: () {
                },
            ),

            ListTile(
              leading: Icon(
                Icons.exit_to_app,
              ),
              title: Text('Logout', style: TextStyle(fontSize: 16.0, fontFamily: 'ProductSans')),
              onTap: () {
                FirebaseAuth.instance.signOut().then((value) {
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => LoginScreen()), (Route<dynamic> route) => false);
                }).catchError((e) {
                  print(e);
                });
              },
            ),

            ListTile(
              leading: Icon(
                Icons.settings,
              ),
              title: Text('Setting', style: TextStyle(fontSize: 16.0, fontFamily: 'ProductSans')),
              onTap: () {

              },
            ),

            Divider(),

            ListTile(
              leading: Icon(
                Icons.date_range,
              ),
              title: Text('Expire date', style: TextStyle(fontSize: 16.0, fontFamily: 'ProductSans')),
              onTap: () {
               },
              trailing: Text("${currentDate.day.toString()} - ${currentDate.month.toString()} - ${currentDate.year.toString()}"),
            ),

            ListTile(
              leading: Icon(
                Icons.today,
              ),
              trailing: Text(
                "15"
              ),
              title: Text('Remaining Days', style: TextStyle(fontSize: 16.0, fontFamily: 'ProductSans')),
              onTap: () {
                },
            ),

            /*
              * All Menu items (to delete Items)
              * */
            ListTile(
              leading: Icon(
                Icons.perm_identity,
              ),
              title: Text('About Us', style: TextStyle(fontSize: 16.0, fontFamily: 'ProductSans')),
              onTap: () {
                },
            ),

          ],
        ),
      ),
    );
  }
}
