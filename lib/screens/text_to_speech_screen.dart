import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class TextToSpeechScreen extends StatefulWidget {

  @override
  _TextToSpeechScreenState createState() => _TextToSpeechScreenState();
}

class _TextToSpeechScreenState extends State<TextToSpeechScreen> {

  TextEditingController speechController = TextEditingController();

  final FlutterTts flutterTts = FlutterTts();

  String speech;

  @override
  Widget build(BuildContext context) {

    Future speak() async{
      print(await flutterTts.getLanguages);
      await flutterTts.setLanguage("hi-IN");
      await flutterTts.setPitch(1);
      await flutterTts.speak(speech);
    }
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            TextFormField(
              controller: speechController,

            ),
            RaisedButton(
              onPressed: (){
                speech = speechController.text;
                speak();
              },
              child: Text("Speek"),
            )
          ],
        ),
      ),
    );
  }
}