import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  String email;
  String name;
  String contact;

  getProfile()async{
    FirebaseAuth auth = FirebaseAuth.instance;
    User user = auth.currentUser;
    DocumentReference doc = FirebaseFirestore.instance.collection('users').doc(user.uid);
    doc.get().then((value) {
      Map<String, dynamic> userData = {
        'full_name': value.data()['full_name'],
        'email': value.data()['email'],
        'contact': value.data()['contact'],
      };
      print(userData['full_name']);
      setState(() {
        nameController = TextEditingController()..text = userData['full_name'];
        emailController = TextEditingController()..text = userData['email'];
        contactController = TextEditingController()..text = userData['contact'];
      });
    });

  }

  updateProfile()async{
    FirebaseAuth auth = FirebaseAuth.instance;
    User user = auth.currentUser;
    DocumentReference doc = FirebaseFirestore.instance
        .collection('users')
        .doc(user.uid);

    Map<String, dynamic> updateUser = {
      'full_name': name,
      'email': email,
      'contact': contact,
    };

    doc
        .set(updateUser)
        .whenComplete(() {
          print('Updated');
          Navigator.pop(context);
    })
        .catchError((e) {
      print(e);
    });
  }
  @override
  void initState() {
    getProfile();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(
              fontSize: 16.0
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Profile",
                style: TextStyle(
                    fontSize: 42.0,
                    fontFamily: 'Productsans',
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.start,
              ),
              Text(
                'Text to speech',
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'Productsans',
                  color: Colors.blue,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 32.0,
              ),
              TextFormField(
                controller: nameController,
                textCapitalization: TextCapitalization.words,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  suffixIcon: Icon(Icons.edit),
                  fillColor: Colors.white,
                  filled: true,
                  hintText: 'Enter Full Name',
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        width: 1.0, color: Colors.blue),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        width: 1.0, color: Colors.black54),
                  ),
                ),
                style: const TextStyle(
                    fontSize: 13.0,
                    fontFamily: 'Productsans',
                    color: Colors.black),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please Enter Full Name';
                  }
                  return null;
                },
                onSaved: (value) {
                  name = value;
                },
              ),
              SizedBox(
                height: 18.0,
              ),
              TextFormField(
                controller: emailController,
                textCapitalization: TextCapitalization.words,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  suffixIcon: Icon(Icons.edit),
                  fillColor: Colors.white,
                  filled: true,
                  hintText: 'Enter Email',
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        width: 1.0, color: Colors.blue),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        width: 1.0, color: Colors.black54),
                  ),
                ),

                style: const TextStyle(
                    fontSize: 13.0,
                    fontFamily: 'Productsans',
                    color: Colors.black),
                validator: (value) {
                  Pattern pattern =
                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regexp = RegExp(pattern);
                  if (value.isEmpty) {
                    return 'Please Enter Email';
                  } else if (!regexp.hasMatch(value)) {
                    return 'Please enter valid Email';
                  }
                  return null;
                },
                onSaved: (value) {
                  email = value;
                },
              ),
              SizedBox(
                height: 18.0,
              ),
              TextFormField(
                controller: contactController,
                textCapitalization: TextCapitalization.words,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  suffixIcon: Icon(Icons.edit),
                  fillColor: Colors.white,
                  filled: true,
                  hintText: 'Enter Contact No.',
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        width: 1.0, color: Colors.blue),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide(
                        width: 1.0, color: Colors.black54),
                  ),
                ),
                style: const TextStyle(
                    fontSize: 13.0,
                    fontFamily: 'Productsans',
                    color: Colors.black),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please Enter Contact No.';
                  }
                  return null;
                },
                onSaved: (value) {
                  contact = value;
                },
              ),
              SizedBox(
                height: 18.0,
              ),
              RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    print("validate");
                    updateProfile();
                  }
                  //loggingDone();
//                                    Navigator.pushReplacement(
//                                        context,
//                                        MaterialPageRoute(
//                                            builder: (context) =>
//                                                LoginScreen()));
                },
                elevation: 5,
                color: Colors.blue,
                textColor: Colors.white,
                padding: const EdgeInsets.all(0.0),
                shape: RoundedRectangleBorder(
                    borderRadius:
                    BorderRadius.circular(30.0)),
                child: Container(
                  width: 220.0,
                  height: 46.0,
                  padding: const EdgeInsets.fromLTRB(
                      40, 10, 40, 10),
                  child: Center(
                    child: Text('Save',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontFamily: 'Productsans',
                            color: Colors.white)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
