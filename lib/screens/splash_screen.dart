import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:text2speech/screens/login_screen.dart';
import 'package:text2speech/widgets/pref.dart';

import 'homepage_screen.dart';


class SplashScreen extends StatefulWidget{

  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
{
  VoidCallback listener;
  bool isFirstTime;

  @override
  void initState() {
    super.initState();

    firstTimeLoggedIn();

  }

  firstTimeLoggedIn() async {
    final pref = await SharedPreferences.getInstance();

    bool value = pref.getBool(Prefs.KEY_IS_LOGGING) ?? false;

    print("Value :-"+ value.toString());

    if(value)
    {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => HomePageScreen()));

    }
    else{
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => LoginScreen()));

    }
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.cyan,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarColor:
      Colors.white, //or set color with: Color(0xFF0000FF)
    ));
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
           Center(
             child: Text(
               "Text To Speech"
             ),
           )
          ],
        ),
      ),
    );
  }
}