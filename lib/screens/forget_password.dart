import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:text2speech/screens/login_screen.dart';
import 'package:text2speech/screens/signup_screen.dart';
import 'package:text2speech/widgets/constants.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  TextEditingController loginController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController couponController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  String email;
  /*
  * Global Instance of GlobalKey(ScaffoldState)
  * */
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  /*
  * Global Instance of FocusNode
  * */
  FocusNode loginFocus = new FocusNode();
  FocusNode passwordFocus = new FocusNode();
  FocusNode couponFocus = new FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              color: Color(0xFF121212),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Container(
                color: Color(0xFFb2ebf2),
                height: 500.0,
                width: 500.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Container(
                        color: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              /*
                                      * Login text
                                      * */
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      Container(
                                        child: Text(
                                          "Reset Password",
                                          style: TextStyle(
                                              fontSize: 42.0,
                                              fontFamily: 'Productsans',
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                      Text(
                                        'Text to speech',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: 'Productsans',
                                          color: Colors.blue,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4.0,
                              ),
                              const SizedBox(height: 50.0),

                              /*
                                      * Enter User Name
                                      * */
                              TextFormField(
                                focusNode: loginFocus,
                                controller: loginController,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: Constants.TEXT_USER_NAME,
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                },
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return Constants.TEXT_USER_NAME;
                                  }
                                  return null;
                                },
                                onSaved: (value){
                                  email = value;
                                },
                              ),
//                              SizedBox(
//                                height: 18.0,
//                              ),
//                              /*
//                                       * Enter Password
//                                       * */
//                              Container(
//                                  height: 55.0,
//                                  child: TextFormField(
//                                    focusNode: passwordFocus,
//                                    controller: passwordController,
//                                    obscureText: _obscureText,
//                                    textCapitalization:
//                                        TextCapitalization.words,
//                                    textInputAction: TextInputAction.next,
//                                    decoration: InputDecoration(
//                                      fillColor: Colors.white,
//                                      filled: true,
//                                      hintText: 'New Password',
//                                      suffixIcon: new GestureDetector(
//                                        onTap: () {
//                                          setState(() {
//                                            _obscureText = !_obscureText;
//                                          });
//                                        },
//                                        child: new Icon(
//                                          _obscureText
//                                              ? Icons.visibility_off
//                                              : Icons.visibility,
//                                          color: Colors.black54,
//                                          size: 22.0,
//                                        ),
//                                      ),
//                                      focusedBorder: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(10),
//                                        borderSide: BorderSide(
//                                            width: 1.0, color: Colors.blue),
//                                      ),
//                                      enabledBorder: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(10),
//                                        borderSide: BorderSide(
//                                            width: 1.0, color: Colors.black54),
//                                      ),
//                                    ),
//                                    style: const TextStyle(
//                                        fontSize: 13.0,
//                                        fontFamily: 'Productsans',
//                                        color: Colors.black),
//                                    onFieldSubmitted: (value) {
//                                      FocusScope.of(context)
//                                          .requestFocus(couponFocus);
//                                    },
//                                    validator: (value) {
//                                      if (value.isEmpty) {
//                                        return Constants.TEXT_PASSWORD;
//                                      }
//                                      return null;
//                                    },
//                                    onChanged: (value) {},
//                                  )),
//                              SizedBox(
//                                height: 18.0,
//                              ),
//                              Container(
//                                  height: 55.0,
//                                  child: TextFormField(
//                                    focusNode: passwordFocus,
//                                    controller: passwordController,
//                                    obscureText: _obscureText,
//                                    textCapitalization:
//                                        TextCapitalization.words,
//                                    textInputAction: TextInputAction.next,
//                                    decoration: InputDecoration(
//                                      fillColor: Colors.white,
//                                      filled: true,
//                                      hintText: 'Re-Enter Password',
//                                      suffixIcon: new GestureDetector(
//                                        onTap: () {
//                                          setState(() {
//                                            _obscureText = !_obscureText;
//                                          });
//                                        },
//                                        child: new Icon(
//                                          _obscureText
//                                              ? Icons.visibility_off
//                                              : Icons.visibility,
//                                          color: Colors.black54,
//                                          size: 22.0,
//                                        ),
//                                      ),
//                                      focusedBorder: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(10),
//                                        borderSide: BorderSide(
//                                            width: 1.0, color: Colors.blue),
//                                      ),
//                                      enabledBorder: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(10),
//                                        borderSide: BorderSide(
//                                            width: 1.0, color: Colors.black54),
//                                      ),
//                                    ),
//                                    style: const TextStyle(
//                                        fontSize: 13.0,
//                                        fontFamily: 'Productsans',
//                                        color: Colors.black),
//                                    onFieldSubmitted: (value) {
//                                      FocusScope.of(context)
//                                          .requestFocus(couponFocus);
//                                    },
//                                    validator: (value) {
//                                      if (value.isEmpty) {
//                                        return Constants.TEXT_PASSWORD;
//                                      }
//                                      return null;
//                                    },
//                                    onChanged: (value) {},
//                                  )),
                              SizedBox(
                                height: 18.0,
                              ),
                              Center(
                                child: RaisedButton(
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      FirebaseAuth.instance.sendPasswordResetEmail(email: email);
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  LoginScreen()));

                                    }
                                    //loggingDone();

                                  },
                                  elevation: 5,
                                  color: Colors.blue,
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0)),
                                  child: Container(
                                    width: 220.0,
                                    height: 46.0,
                                    padding: const EdgeInsets.fromLTRB(
                                        40, 10, 40, 10),
                                    child: Center(
                                      child: Text('Reset Password',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontFamily: 'Productsans',
                                              color: Colors.white)),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
