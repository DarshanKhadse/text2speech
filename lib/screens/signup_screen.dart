import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:text2speech/screens/login_screen.dart';
import 'package:text2speech/widgets/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  /*
  * Global Instance of GlobalKey(ScaffoldState)
  * */
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmController = TextEditingController();
  String full_name;
  String email;
  String phone;
  String password;
  String confirm_password;

  FocusNode loginFocus = new FocusNode();
  FocusNode passwordFocus = new FocusNode();
  FocusNode couponFocus = new FocusNode();

  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              color: Color(0xFF121212),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Container(
                color: Color(0xFFb2ebf2),
                height: 500.0,
                width: 500.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Container(
                        color: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              /*
                                      * Login text
                                      * */
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      Container(
                                        child: Text(
                                          "Signup",
                                          style: TextStyle(
                                              fontSize: 42.0,
                                              fontFamily: 'Productsans',
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.start,
                                        ),
                                      ),
                                      Text(
                                        'Text to speech',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: 'Productsans',
                                          color: Colors.blue,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4.0,
                              ),
                              const SizedBox(height: 50.0),

                              /*
                                      * Enter User Name
                                      * */
                              TextFormField(
                                controller: nameController,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: 'Enter Full Name',
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                },
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please Enter Full Name';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  full_name = value;
                                },
                              ),
                              SizedBox(
                                height: 18.0,
                              ),
                              TextFormField(
                                controller: emailController,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: 'Enter Email',
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                },
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                validator: (value) {
                                  Pattern pattern =
                                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regexp = RegExp(pattern);
                                  if (value.isEmpty) {
                                    return 'Please Enter Email';
                                  } else if (!regexp.hasMatch(value)) {
                                    return 'Please enter valid Email';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  email = value;
                                },
                              ),
                              SizedBox(
                                height: 18.0,
                              ),
                              TextFormField(
                                controller: contactController,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: 'Enter Contact No.',
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                },
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please Enter Contact No.';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  phone = value;
                                },
                              ),
                              SizedBox(
                                height: 18.0,
                              ),
                              TextFormField(
                                controller: passwordController,
                                obscureText: _obscureText,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: 'Enter Password',
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    child: new Icon(
                                      _obscureText
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: Colors.black54,
                                      size: 22.0,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocus);
                                },
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please Enter Password';
                                  } else if (value.length <= 7) {
                                    return 'Password should be greater than 8';
                                  }
                                  _formKey.currentState.save();
                                  return null;
                                },
                                onSaved: (value) {
                                  password = value;
                                },
                              ),
                              SizedBox(
                                height: 18.0,
                              ),
                              /*
                                       * Enter Password
                                       * */
                              TextFormField(
                                controller: confirmController,
                                obscureText: _obscureText,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintText: 'Confirm Pasword',
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    child: new Icon(
                                      _obscureText
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: Colors.black54,
                                      size: 22.0,
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.blue),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.black54),
                                  ),
                                ),
                                style: const TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Productsans',
                                    color: Colors.black),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(couponFocus);
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please Enter Password';
                                  } else if (value.length < 7) {
                                    return 'Password should be greater than 8 digit';
                                  } else if (value != password) {
                                    return 'Password not Matched!';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  confirm_password = value;
                                },
                              ),
                              SizedBox(
                                height: 18.0,
                              ),
                              Center(
                                child: RaisedButton(
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      print(phone);
                                      print(confirm_password);

                                      signUp();
                                    }
                                    //loggingDone();
//                                    Navigator.pushReplacement(
//                                        context,
//                                        MaterialPageRoute(
//                                            builder: (context) =>
//                                                LoginScreen()));
                                  },
                                  elevation: 5,
                                  color: Colors.blue,
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0)),
                                  child: Container(
                                    width: 220.0,
                                    height: 46.0,
                                    padding: const EdgeInsets.fromLTRB(
                                        40, 10, 40, 10),
                                    child: Center(
                                      child: Text('Signup',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontFamily: 'Productsans',
                                              color: Colors.white)),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 14,
                              ),
                              Center(
                                child: RichText(
                                  text: TextSpan(
                                      text: 'Already Have an Account? ',
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black,
                                      ),
                                      children: [
                                        TextSpan(
                                            text: 'Login Here!',
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blue),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            LoginScreen()));
                                              }),
                                      ]),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  signUp() async {
    FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((signInUser) {
      DocumentReference doc = FirebaseFirestore.instance
          .collection('users')
          .doc(signInUser.user.uid);

      Map<String, dynamic> user = {
        'full_name': full_name,
        'email': email,
        'contact': phone,
      };

      doc
          .set(user)
          .whenComplete(() => Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginScreen())))
          .catchError((e) {
        print(e);
      });
    });
  }
}
